import React from 'react'
import {
  Box,
  Button,
  Container,
  TextField,
  Typography,
  // Link,
} from '@mui/material'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useAuth } from '../hooks/useAuth'

const validation = yup.object().shape({
  username: yup.string().required('Username is required'),
  password: yup.string().required('Password is required'),
})

interface LoginFormsInputs {
  username: string
  password: string
}

const Login: React.FC = () => {
  const { loginUser } = useAuth()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormsInputs>({
    resolver: yupResolver(validation),
  })

  const handleLogin = (form: LoginFormsInputs) => {
    loginUser(form.username, form.password)
  }

  return (
    <Box
      sx={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0f0e27',
      }}
    >
      <Container maxWidth="xs">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: '40px',
            borderRadius: '8px',
            backgroundColor: '#212040',
            boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
          }}
        >
          <Typography
            sx={{
              fontSize: '2rem',
              fontWeight: 'bold',
              color: '#ffffff',
              marginBottom: '20px',
            }}
          >
            ครูแหว๋ว มินิมาร์ท
          </Typography>
          <form onSubmit={handleSubmit(handleLogin)}>
            <TextField
              label="Username"
              variant="outlined"
              fullWidth
              {...register('username')}
              error={!!errors.username}
              helperText={errors.username ? errors.username.message : ''}
              sx={{
                marginBottom: '20px',
                '& .MuiInputBase-input': {
                  color: '#ffffff',
                },
                '& .MuiInputLabel-root': {
                  color: '#ffffff',
                },
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: '#ffffff',
                  },
                  '&:hover fieldset': {
                    borderColor: '#ffffff',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#ffffff',
                  },
                },
              }}
            />
            <TextField
              label="Password"
              type="password"
              variant="outlined"
              fullWidth
              {...register('password')}
              error={!!errors.password}
              helperText={errors.password ? errors.password.message : ''}
              sx={{
                marginBottom: '20px',
                '& .MuiInputBase-input': {
                  color: '#ffffff',
                },
                '& .MuiInputLabel-root': {
                  color: '#ffffff',
                },
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: '#ffffff',
                  },
                  '&:hover fieldset': {
                    borderColor: '#ffffff',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#ffffff',
                  },
                },
              }}
            />
            <Button
              type="submit"
              variant="contained"
              fullWidth
              sx={{
                backgroundColor: '#fff',
                color: '#000',
                '&:hover': {
                  backgroundColor: '#0f0e27',
                  color: '#fff',
                },
              }}
            >
              LOG IN
            </Button>
          </form>
          {/* <Link
            href="#"
            sx={{
              marginTop: '20px',
              color: '#ffffff',
              textDecoration: 'none',
            }}
          >
            FORGOT YOUR PASSWORD?
          </Link> */}
        </Box>
      </Container>
    </Box>
  )
}

export default Login
