import ReactDOM from 'react-dom/client'
import './index.css'
import { ThemeProvider } from '@emotion/react'
import { CssBaseline } from '@mui/material'
import { AppRoute } from './routes.tsx'
import theme from './theme.tsx'
import { UserProvider } from './hooks/useAuth.tsx'
import { BrowserRouter } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <BrowserRouter>
      <UserProvider>
        <AppRoute />
      </UserProvider>
    </BrowserRouter>
  </ThemeProvider>
)
