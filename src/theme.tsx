import { createTheme } from '@mui/material/styles'
import { red } from '@mui/material/colors'

// Define the custom keyframes animation
const gradientKeyframes = {
  '@keyframes gradient': {
    '0%': {
      backgroundPosition: '0% 50%',
    },
    '50%': {
      backgroundPosition: '100% 50%',
    },
    '100%': {
      backgroundPosition: '0% 50%',
    },
  },
}

// A custom theme for this app
const theme = createTheme(
  {
    typography: {
      fontFamily: '"Mitr", sans-serif',
      fontWeightLight: 100,
      fontWeightRegular: 400,
      fontWeightMedium: 500,
      fontWeightBold: 700,
    },
    palette: {
      primary: {
        main: '#FFFFFF',
      },
      secondary: {
        main: '#252525',
      },
      error: {
        main: red.A400,
      },
    },
  },
  () => ({
    // Include the custom keyframes animation
    '@global': {
      ...gradientKeyframes,
    },
  })
)

export default theme
