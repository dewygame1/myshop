import axios from 'axios'
import { UserProfileToken } from '../models/UserModel'
import { handleError } from '../helpers/ErrorHandle'
import axiosInstance from '../api/axiosConfig'

export const loginAPI = async (username: string, password: string) => {
  try {
    const data = await axiosInstance.post<UserProfileToken>('/login', {
      username: username,
      password: password,
    })
    return data
  } catch (error) {
    handleError(error)
  }
}

export const registerAPI = async (
  email: string,
  username: string,
  password: string
) => {
  try {
    const data = await axios.post<UserProfileToken>('/register', {
      email: email,
      username: username,
      password: password,
    })
    return data
  } catch (error) {
    handleError(error)
  }
}

export const logoutAPI = async () => {
  try {
    const response = await axiosInstance.post('/logout')
    return response
  } catch (error) {
    handleError(error)
  }
}
