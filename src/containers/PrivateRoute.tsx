import { Routes, Route, Navigate } from 'react-router-dom'

// import MasterLayout from '../containers/Layouts/MasterLayout'
import Home from '../components/Home'
import Dashboard from './Layouts/MasterLayout2'

const PrivateRoute = () => {
  return (
    <Routes>
      <Route element={<Dashboard />}>
        <Route path="home" element={<Home />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Route>
    </Routes>
  )
}

export default PrivateRoute
