import { Routes, Route, Navigate } from 'react-router-dom'
import LoginPage from '../components/LoginPage'

const PublicRoute = () => {
  return (
    <Routes>
      <Route path="login" element={<LoginPage />} />
      <Route path="*" element={<Navigate to="login" />} />
    </Routes>
  )
}

export default PublicRoute
