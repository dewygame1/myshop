import { Box, Unstable_Grid2 as Grid } from '@mui/material'
import { Outlet } from 'react-router-dom'
const AuthLayout = () => {
  return (
    <>
      {/* <div>AuthLayout</div> */}

      <Box
        component="main"
        sx={{
          display: 'flex',
          flex: '1 1 auto',
        }}
      >
        <Grid container sx={{ flex: '1 1 auto' }}>
          <Grid
            xs={12}
            lg={6}
            lgOffset={3}
            sx={{
              backgroundColor: 'background.paper',
              display: 'flex',
              flexDirection: 'column',
              position: 'relative',
            }}
          >
            <Outlet />
          </Grid>
        </Grid>
      </Box>
    </>
  )
}

export default AuthLayout
