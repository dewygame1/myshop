// AppRoute.tsx

import { FC } from 'react'
import { Routes, Route } from 'react-router-dom'
import PrivateRoute from './containers/PrivateRoute'
import PublicRoute from './containers/PublicRoute'
import { useAuth } from './hooks/useAuth'
import App from './containers/App'
import Error404 from './containers/Error404'

const AppRoute: FC = () => {
  const { isLoggedIn } = useAuth()

  //   if (loading) {
  //     return (
  //       <Box
  //         display="flex"
  //         justifyContent="center"
  //         alignItems="center"
  //         height="100vh"
  //       >
  //         <CircularProgress />
  //       </Box>
  //     )
  //   }

  return (
    <Routes>
      <Route element={<App />}>
        <Route path="error/*" element={<Error404 />} />

        {isLoggedIn() ? (
          <>
            <Route path="*" element={<PrivateRoute />} />
          </>
        ) : (
          <>
            <Route path="*" element={<PublicRoute />} />
          </>
        )}
      </Route>
    </Routes>
  )
}

export { AppRoute }
